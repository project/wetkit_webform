; WetKit Webform Makefile

api = 2
core = 7.x

; Modules

projects[webform][version] = 4.25
projects[webform][subdir] = contrib

projects[webform_uuid][version] = 1.3
projects[webform_uuid][subdir] = contrib
